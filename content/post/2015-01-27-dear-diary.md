---
title: Dear diary
date: 2015-01-27
bigimg:
- src: "/uploads/2019/06/impossible.320-150x150@2x.jpg"
  desc: Impossible!
subtitle: ''

---
What is it with that Mary girl?  Dragging me to school every day. As if I had a choice.  What you don't hear in those nursery rhymes is that she starves me if I don't go to school with her; it's the only way I can stay alive!  I'm thinking about being adopted by Little Bo Peep, sure I may get lost, but anything is better than being with Mary and those little brats at school (shudder, shudder).